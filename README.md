# The Dyna Cryptosystem #

The List Of Programs Is As Follwed:

* The Ceaser Cipher


```
#!python

#----------------------------------------------------------------------------------#
# Project : The Dyna Cryptosystem                                                  #
# Script  : The ROT13 Cipher | Ceaser Cipher                                       #
# Author  : Mrinal Wahal                                                           #
# Website : http://www.dynacrux.de.vu/                                             #
#----------------------------------------------------------------------------------#

```

* The ADFGX Cipher



```
#!python

#----------------------------------------------------------------------------------#
# Project : The Dyna Cryptosystem                                                  #
# Script  : The ADFGX Cipher                                                       #
# Author  : Mrinal Wahal                                                           #
# Website : http://www.dynacrux.de.vu/                                             #
#----------------------------------------------------------------------------------#

```

* The Obfuscator


```
#!python

#---------------------------------------------------------------------#
# Project : The Dyna Cryptosystem                                     #
# Script  : Obfuscator                                                #
# Author  : Mrinal Wahal                                              #
# Website : http://www.dynacrux.de.vu/                                #
#---------------------------------------------------------------------#

```


### Designed & Deployed on 11th August, 2014.

### Mrinal Wahal#