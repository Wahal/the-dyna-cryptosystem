#----------------------------------------------------------------------------------#
# Project : The Dyna Cryptosystem                                                  #
# Script  : The ROT13 Cipher | Ceaser Cipher                                       #
# Author  : Mrinal Wahal                                                           #
# Website : http://www.dynacrux.de.vu/                                             #
#----------------------------------------------------------------------------------#

print "-"*60
print "The ROT13 Cipher | Ceaser Cipher"
print "-"*60

alphas = {}
bets = 'abcdefghijklmnopqrstuvwxyz'

string = raw_input("\nEnter The Message - ")
print "\nNOTE - Value 0 Will Assign Default Rotation of 13 Places."
rotation = input("\nRotation Should Take Place By - ")

num = 0

for letter in bets:
    num += 1
    alphas[letter]=num

def encrypt(string,rotation):
    encrypted = ""
    for let in string:
        if rotation == 0:
            rotation = 13

        newvalue = alphas[let] + rotation
        if newvalue > 26:
            newvalue -= 26
        for key in alphas.iterkeys():
            if alphas[key] == newvalue:
                print key,
                encrypted += key
    return encrypted
print "\nThe Encrypted String is :\n"

en = encrypt(string,rotation)

def decrypt(dstring,drotation):
    for dlet in str(dstring):
        if drotation == 0:
            drotation = 13

        dnewvalue = alphas[dlet] - drotation
        if dnewvalue < 0:
            dnewvalue += 26
        for dkey in alphas.iterkeys():
            if alphas[dkey] == dnewvalue:
                print dkey,

print "\n\nThe Decrypted String is :\n"
decrypt(en,rotation)

quit = raw_input("\n\nPress Any Key To Terminate...")
